﻿using System;
using System.Collections.Generic;

namespace Libs.AdWrapper.Models
{
    public struct AdPlace
    {
        public string Place { get; }

        public AdPlace(string place)
        {
            Place = place;
        }

        public object GetEvent()
        {
            var data = Place.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length == 1)
                return Place;
            if (data.Length > 2)
                throw new Exception("[AdPlace] Event keys more 2! Event: " + Place);
            return new Dictionary<string, object> {{data[0], data[1]}};
        }

        public override bool Equals(object obj)
        {
            var value = obj as AdPlace?;
            return value.HasValue && Equals(value.Value);
        }

        public bool Equals(AdPlace other)
        {
            return string.Equals(Place, other.Place);
        }

        public override int GetHashCode()
        {
            return (Place != null ? Place.GetHashCode() : 0);
        }

        public override string ToString()
        {
            return $"{nameof(Place)}: {Place}";
        }
    }
}