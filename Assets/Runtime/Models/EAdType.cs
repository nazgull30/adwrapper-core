﻿namespace Libs.AdWrapper.Models
{
    public enum EAdType
    {
        Banner,
        Interstitial,
        Video
    }
}