namespace Libs.AdWrapper.Models
{
    public enum ESdkType
    {
        Emulator = 1, 
        AdMob = 2, 
        Appodeal = 3,
        Mopub = 4,
        Yodo1 = 5
    }
}