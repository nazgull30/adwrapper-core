using Libs.AdWrapper.Impl;
using Libs.AdWrapper.Models;
using Libs.Analytics;

namespace Libs.AdWrapper.Signals
{
	public struct SignalAd : ISignalAnalytic
	{
		public readonly AdPlace? AdPlace;
		public readonly EAdType AdType;
		public readonly EAdState State;
		public readonly ESdkType SdkType;
		public readonly IAdResponse AdResponse;
		public readonly double? Duration;
		public string Error;
		public bool? NetworkOrHttpError;

		public SignalAd(AdPlace? adPlace, EAdState state, ESdkType sdkType, EAdType adType, IAdResponse adResponse, double? duration) : this()
		{
			AdPlace = adPlace;
			State = state;
			SdkType = sdkType;
			AdType = adType;
			AdResponse = adResponse;
			Duration = duration;
		}
	}
}