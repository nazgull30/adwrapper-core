using System.Collections.Generic;

namespace Libs.AdWrapper.Impl
{
	public interface IAdResponse
	{
		Dictionary<string, object> GetArguments();
	}
	
	public class AdResponse : IAdResponse
	{
		public static readonly AdResponse Empty = new AdResponse();
		
		private AdResponse()
		{
			
		}

		public Dictionary<string, object> GetArguments() => new Dictionary<string, object>();
	}
}