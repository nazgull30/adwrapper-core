using System;
using Libs.AdWrapper.Models;
using UniRx;
using UnityEngine;

namespace Libs.AdWrapper.Impl.Emulator
{
    public class AdManagerEmulator : IInterstitialManager, IRewardVideoManager, IDisposable
    {
        private readonly CompositeDisposable _disposables = new CompositeDisposable();
        private readonly IAdEmulatorSettings _emulatorSettings;
        
        private Action<EAdState, string, IAdResponse> _state;
        private bool _isLoaded;

        private int _failCount;

        public AdManagerEmulator(IAdEmulatorSettings emulatorSettings)
        {
            _emulatorSettings = emulatorSettings;
        }

        public void RequestAllow(IAdRequest adRequest, Action success) { }
        
        public bool IsAdsForbidden => false;
        
        public void Configure() { }
        
        public bool PreloadAutomatically => false;

        public void ListenState(Action<EAdState, string, IAdResponse> state) => _state = state;

        public ESdkType GetSdkType() => ESdkType.Emulator;
        public EAdType GetAdType() => _emulatorSettings.AdType;

        public void Dispose()
        {
            _disposables?.Clear();
        }

        public bool IsLoaded(IAdRequest adRequest) => _isLoaded;

        public void Preload(IAdRequest adRequest)
        {
            _state?.Invoke(EAdState.Preload, string.Empty, AdResponse.Empty);
            Observable.Timer(TimeSpan.FromSeconds(_emulatorSettings.DelayTimeS)).Subscribe(_ =>
                {
                    var success = _emulatorSettings.LoadFailsInRow == 0 || _failCount >= _emulatorSettings.LoadFailsInRow;
                    if (success)
                    {
                        _isLoaded = _emulatorSettings.AdState != EAdState.Fail && _emulatorSettings.AdState != EAdState.Forbidden;
                        var adState = _isLoaded ? EAdState.Loaded : EAdState.Fail;
                        _state?.Invoke(adState, string.Empty, AdResponse.Empty);
                        _failCount = 0;
                    }
                    else
                    {
                        _state?.Invoke(EAdState.Fail, string.Empty, AdResponse.Empty);
                        _failCount++;
                    }
                    Debug.Log("[AdManagerEmulator] Preload " + GetAdType());
                })
                .AddTo(_disposables);
        }

        public void Show(IAdRequest adRequest)
        {
            Debug.Log("[AdManagerEmulator] Show ad of type " + _emulatorSettings.AdType + " and complete");
            _state?.Invoke(EAdState.Open, string.Empty, AdResponse.Empty);
            _state?.Invoke(EAdState.Complete, string.Empty, AdResponse.Empty);
            _state?.Invoke(EAdState.Close, string.Empty, AdResponse.Empty);
            _isLoaded = false;
        }
    }
}