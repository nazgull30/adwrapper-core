using Libs.AdWrapper.Models;

namespace Libs.AdWrapper.Impl.Emulator
{
	public interface IAdEmulatorSettings
	{
		EAdState AdState { get; }
		EAdType AdType { get; }
		float DelayTimeS { get; }
		int LoadFailsInRow { get; }
	}
}