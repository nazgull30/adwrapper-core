using Libs.AdWrapper.Models;
using Libs.AdWrapper.StateListeners;

namespace Libs.AdWrapper.Impl
{
	public interface IAdRequest
	{
		AdPlace? GetPlace();
		Listener GetListener();
	}

	public struct AdRequest : IAdRequest
	{
		public static readonly IAdRequest Empty = new AdRequest();
		
		private readonly AdPlace? _place;
		private readonly Listener _listener;

		private AdRequest(AdPlace? place, Listener listener)
		{
			_place = place;
			_listener = listener;
		}

		public AdRequest(AdPlace? place) : this()
		{
			_place = place;
		}

		public static AdRequest Create(string place, IStateListener stateListener)
		{
			var adPlace = new AdPlace(place);
			return Create(adPlace, stateListener);
		}
		
		public static AdRequest Create(AdPlace place, IStateListener stateListener)
		{
			var listener = Listener.Create(stateListener);
			return new AdRequest(place, listener);
		}

		public AdPlace? GetPlace() => _place;

		public Listener GetListener() => _listener;
	}
}