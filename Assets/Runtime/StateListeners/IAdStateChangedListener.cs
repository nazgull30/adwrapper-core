using Libs.AdWrapper.Impl;
using Libs.AdWrapper.Models;

namespace Libs.AdWrapper.StateListeners
{
	public interface IAdStateChangedListener : IStateListener
	{
		void HandleChangeState(EAdState newState, IAdRequest adRequest);
	}
}