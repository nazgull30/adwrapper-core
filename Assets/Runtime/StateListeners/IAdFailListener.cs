namespace Libs.AdWrapper.StateListeners
{
	public interface IAdFailListener : IStateListener
	{
		void Handle(bool networkOrHttpError);
	}
}