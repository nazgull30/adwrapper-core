namespace Libs.AdWrapper.StateListeners
{
	public interface IAdLoadedListener : IStateListener
	{
		void Handle();
	}
}