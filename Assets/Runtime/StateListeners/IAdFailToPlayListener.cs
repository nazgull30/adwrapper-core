namespace Libs.AdWrapper.StateListeners
{
	public interface IAdFailToPlayListener : IStateListener
	{
		void Handle(bool networkOrHttpError);
	}
}