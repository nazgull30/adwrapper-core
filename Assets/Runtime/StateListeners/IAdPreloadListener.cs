namespace Libs.AdWrapper.StateListeners
{
	public interface IAdPreloadListener : IStateListener
	{
		void Handle();
	}
}