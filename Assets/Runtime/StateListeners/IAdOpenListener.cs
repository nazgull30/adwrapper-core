namespace Libs.AdWrapper.StateListeners
{
	public interface IAdOpenListener : IStateListener
	{
		void Handle();
	}
}