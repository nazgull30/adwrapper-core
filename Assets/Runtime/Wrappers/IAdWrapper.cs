using System;
using Libs.AdWrapper.Impl;
using Libs.AdWrapper.Models;

namespace Libs.AdWrapper.Wrappers
{
	public interface IAdWrapper : IAdBasic
	{
		void Configure();

		void SetChangeStateAction(Action<EAdState, IAdRequest> changeStateAction);
		
		bool IsAdsForbidden { get; }
		void RequestAllowAds(IAdRequest request, Action success);
		void PreloadOnly();
	}
}