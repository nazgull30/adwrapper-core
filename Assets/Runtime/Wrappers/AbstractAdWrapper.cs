using System;
using Libs.AdWrapper.Impl;
using Libs.AdWrapper.Models;
using Libs.AdWrapper.Signals;
using PdUtils;
using UnityEngine;
using Zenject;

namespace Libs.AdWrapper.Wrappers
{
	public abstract class AbstractAdWrapper : IAdWrapper
	{
		private readonly IAdManager _adManager;
		private readonly SignalBus _signalBus;

		private IAdRequest _adRequest;
		private bool _isWaitPreload;
		private bool _isLoaded;
		private DateTime _dateTime;
		
		private bool _receivedCompleted;
		private bool _showImmediately;

		private Action<EAdState, IAdRequest> _changeStateAction;

		protected AbstractAdWrapper(IAdManager adManager, SignalBus signalBus)
		{
			_adManager = adManager;
			_signalBus = signalBus;
		}

		public virtual void Configure()
		{
			_adManager.Configure();
			_adManager.ListenState(OnChangeState);
		}

		public void SetChangeStateAction(Action<EAdState, IAdRequest> changeStateAction) =>
			_changeStateAction = changeStateAction;

		public bool IsLoaded(IAdRequest adRequest) => _adManager.IsLoaded(adRequest);

		public bool IsAdsForbidden => _adManager.IsAdsForbidden;
		
		public void RequestAllowAds(IAdRequest request, Action success) => _adManager.RequestAllow(request, success);

		public void PreloadOnly()
		{
			if(!_adManager.PreloadAutomatically) 
				return;
			InternalPreload(AdRequest.Empty, false);
		}

		public void Preload(IAdRequest adRequest) => InternalPreload(adRequest, true);

		private void InternalPreload(IAdRequest adRequest, bool showImmediately)
		{
			Debug.Log("[AbstractAdWrapper] InternalPreload on place -> _isLoaded: " + _isLoaded + ", _isWaitPreload: " + _isWaitPreload
			 + ", showImmediately: " + showImmediately);
			_showImmediately = showImmediately;
			_adRequest = adRequest;
			if (_isLoaded || _isWaitPreload)
			{
				Debug.Log("[AbstractAdWrapper] InternalPreload on place " + adRequest.GetPlace() + ", _isLoaded and _isWaitPreload should be false");
				return;
			}
			_isWaitPreload = true;
			_adManager.Preload(adRequest);
		}

		public void Show(IAdRequest adRequest)
		{
			Debug.Log("[AbstractAdWrapper] Show on place " + adRequest.GetPlace() + ", _isLoaded: " + _isLoaded);
			_showImmediately = true;
			_adRequest = adRequest;
			if (!_isLoaded)
			{
				Debug.Log("[AbstractAdWrapper] Show on place " + adRequest.GetPlace() + ", _isLoaded should be true");
				return;	
			}
			_receivedCompleted = false;
			_isLoaded = false;
			_dateTime = DateTime.UtcNow;
			_adManager.Show(adRequest);
		}
	
		private void OnChangeState(EAdState state, string error, IAdResponse adResponse)
		{
			Debug.Log("[AbstractAdWrapper] OnChangeState " + state + ", _showImmediately: " + _showImmediately);
			if (_showImmediately)
			{
				HandleStateChangeImmediatelyShow(state, error, adResponse);
			}
			else
			{
				HandleStateChangeDelayedShow(state, error);
			}

		}

		private void HandleStateChangeDelayedShow(EAdState state, string error)
		{
			Debug.Log("[AbstractAdWrapper] HandleStateChangeDelayedShow: " + state + ", error: " + error);

			if (state != EAdState.Preload)
			{
				_isWaitPreload = false;
			}
			
			if (state == EAdState.Loaded)
			{
				_isLoaded = true;
			}

			if (state == EAdState.Fail)
			{
				_isLoaded = false;
				InternalPreload(_adRequest, false);
			}
		}
		
		private void HandleStateChangeImmediatelyShow(EAdState state, string error, IAdResponse adResponse)
		{
			Debug.Log("[AbstractAdWrapper] HandleStateChangeImmediately: " + state + ", error: " + error);

			switch (state)
			{
				case EAdState.None:
					break;
				case EAdState.Open:
					HandleOpen(adResponse);
					break;
				case EAdState.Preload:
					HandlePreload(adResponse);
					break;
				case EAdState.Fail:
					HandleFail(adResponse, error);
					break;
				case EAdState.Forbidden:
					HandleForbidden(adResponse);
					break;
				case EAdState.Loaded:
					HandleLoaded(adResponse);
					break;
				case EAdState.Complete:
					HandleComplete(adResponse);
					break;
				case EAdState.Close:
					HandleClose(adResponse);
					break;
				case EAdState.FailToPlay:
					HandleFailToPlay(adResponse, error);
					break;
				case EAdState.Expire:
					HandleExpire(adResponse);
					break;
				case EAdState.Clicked:
					HandleClick(adResponse);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(state), state, null);
			}
		}
		
		private void OnFailed(Action<bool> fireSignalAction)
		{
			NetworkConnectionChecker.Check(() =>
			{
				fireSignalAction?.Invoke(false);
			}, () =>
			{
				fireSignalAction?.Invoke(true);
			});
		}
		
		private void HandleOpen(IAdResponse adResponse)
		{
			_adRequest?.GetListener()?.OpenListener?.Handle();
			HandleChangeStates(EAdState.Open);
			var signal = CreateSignal(EAdState.Open, adResponse);
			_signalBus.Fire(signal);
		}

		private void HandlePreload(IAdResponse adResponse)
		{
			_dateTime = DateTime.UtcNow;
			_adRequest?.GetListener()?.PreloadListener?.Handle();
			HandleChangeStates(EAdState.Preload);
			var signal = CreateSignal(EAdState.Preload, adResponse);
			_signalBus.Fire(signal);
		}

		private void HandleFail(IAdResponse adResponse, string error)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			OnFailed(networkOrHttpError =>
			{
				var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
				_adRequest?.GetListener()?.FailListener?.Handle(networkOrHttpError);
				HandleChangeStates(EAdState.Fail);
				var signal = CreateSignal(EAdState.Fail, adResponse, duration);
				signal.Error = error;
				signal.NetworkOrHttpError = networkOrHttpError;
				_signalBus.Fire(signal);
			});
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);	
			}
		}
		
		private void HandleFailToPlay(IAdResponse adResponse, string error)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			OnFailed(networkOrHttpError =>
			{
				var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
				_adRequest?.GetListener()?.FailToPlayListener?.Handle(networkOrHttpError);
				var signal = CreateSignal(EAdState.FailToPlay, adResponse, duration);
				HandleChangeStates(EAdState.FailToPlay);
				signal.Error = error;
				signal.NetworkOrHttpError = networkOrHttpError;
				_signalBus.Fire(signal);
			});
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);	
			}
		}

		private void HandleForbidden(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			_adRequest?.GetListener()?.ForbiddenListener?.Handle();
			HandleChangeStates(EAdState.Forbidden);
			var signal = CreateSignal(EAdState.Forbidden, adResponse, duration);
			_signalBus.Fire(signal);
		}

		private void HandleLoaded(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = true;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			_adRequest?.GetListener()?.LoadedListener?.Handle();
			HandleChangeStates(EAdState.Loaded);
			var signal = CreateSignal(EAdState.Loaded, adResponse, duration);
			_signalBus.Fire(signal);
		}

		private void HandleComplete(IAdResponse adResponse)
		{
			_receivedCompleted = true;
			_isWaitPreload = false;
			_isLoaded = false;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			_adRequest?.GetListener()?.CompleteListener?.Handle();
			HandleChangeStates(EAdState.Complete);
			var signal = CreateSignal(EAdState.Complete, adResponse, duration);
			_signalBus.Fire(signal);
		}

		private void HandleClick(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			var signal = CreateSignal(EAdState.Clicked, adResponse);
			_signalBus.Fire(signal);
		}
		
		private void HandleClose(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			if (!_receivedCompleted) // send close only if no completed
			{
				_adRequest?.GetListener()?.CloseListener?.Handle();	
			}
			HandleChangeStates(EAdState.Close);
			var signal = CreateSignal(EAdState.Close, adResponse, duration);
			_signalBus.Fire(signal);
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);	
			}
		}

		private void HandleExpire(IAdResponse adResponse)
		{
			var signal = CreateSignal(EAdState.Expire, adResponse);
			_signalBus.Fire(signal);
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);	
			}
		}

		private void HandleChangeStates(EAdState adState)
		{
			_adRequest?.GetListener()?.StateChangedListener?.HandleChangeState(adState, _adRequest);
			_changeStateAction?.Invoke(adState, _adRequest);
		}

		private SignalAd CreateSignal(EAdState adState, IAdResponse adResponse, double? duration = null)
		{
			var sdkType = _adManager.GetSdkType();
			var adType = _adManager.GetAdType();
			return new SignalAd(_adRequest?.GetPlace(), adState, sdkType, adType, adResponse, duration);
		}
	}
}