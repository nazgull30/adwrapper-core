using Zenject;

namespace Libs.AdWrapper.Wrappers.Impl
{
	public class RewardVideoWrapper : AbstractAdWrapper, IRewardVideoWrapper
	{
		protected RewardVideoWrapper(IRewardVideoManager adManager, SignalBus signalBus) 
			: base(adManager, signalBus)
		{
		}
	}
}