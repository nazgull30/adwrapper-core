using Libs.AdWrapper.Impl.Emulator;
using Libs.AdWrapper.Signals;
using Libs.AdWrapper.Wrappers;
using Libs.AdWrapper.Wrappers.Impl;
using Zenject;

namespace Libs.AdWrapper.Installers
{
    public class AdWrapperInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<SignalAd>();
#if UNITY_EDITOR
            Container.BindInterfacesTo<AdManagerEmulator>().AsSingle();
#endif
	        Container.Bind<IInterstitialWrapper>().To<InterstitialWrapper>().AsSingle();
            Container.Bind<IRewardVideoWrapper>().To<RewardVideoWrapper>().AsSingle();
        }
    }
}