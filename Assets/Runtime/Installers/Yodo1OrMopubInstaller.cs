// using Libs.AdWrapper.Impl.Yodo1;
// using UnityEngine;
//
// namespace Libs.AdWrapper.Installers
// {
// 	public class Yodo1OrMopubInstaller : MopubInstaller
// 	{
// 		public static bool BindCompleted;
// 		[SerializeField] private Yodo1Settings yodoSettings;
//
// 		public override void InstallBindings()
// 		{
// #if !UNITY_EDITOR
// 			Core.Utils.NetworkConnectionChecker.Check(() =>
// 			{
// 				Debug.Log("[Yodo1OrMopubInstaller], There is an internet connection. Init Yodo1");
// 				Container.Bind<Yodo1Settings>().FromInstance(yodoSettings).AsSingle();
// 				Container.BindInterfacesAndSelfTo<Impl.Mopub.MopubConsentDialog>().AsSingle();
// 				// Container.BindInterfacesAndSelfTo<Yodo1ConsentDialog>().AsSingle();
// 				Container.BindInterfacesTo<Libs.AdWrapper.Impl.Yodo1.Impl.Yodo1InterstitialManager>().AsSingle()
// 					.WhenInjectedInto<Libs.AdWrapper.Wrappers.Impl.InterstitialWrapper>();
// 				Container.BindInterfacesTo<Libs.AdWrapper.Impl.Yodo1.Impl.Yodo1RewardVideoManager>().AsSingle()
// 					.WhenInjectedInto<Libs.AdWrapper.Wrappers.Impl.RewardVideoWrapper>();
// 				BindCompleted = true;
// 			}, () =>
// 			{
// 				Debug.Log("[Yodo1OrMopubInstaller], There is no internet connection. Init Mopub");
// 				InstallMopubBindings();
// 				BindCompleted = true;
// 			});
// #else
// 			BindCompleted = true;
// #endif
// 		}
// 	}
// }