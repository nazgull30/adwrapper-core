using System;

namespace Libs.AdWrapper
{
	public interface IConsentDialog
	{
		void Configure();
		
		bool IsRequired { get; }

		void RequestAllow(Action success, Action refuse);
	}

	public static class ConsentConstants
	{
		public const string ConsentPassedKey = "ConsentPassed";
	}
}